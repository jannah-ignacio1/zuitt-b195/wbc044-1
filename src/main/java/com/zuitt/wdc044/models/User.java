package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")

public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user") //One user to Many posts
    //important, otherwise it will recur an infinite nested data
    @JsonIgnore
    private Set<Post> posts;

    //getting post from one user
    public Set<Post> getPosts(){
        return posts;
    }

    //Default constructor
    public User(){}

    //Parameterized constructor
    public  User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }
}
